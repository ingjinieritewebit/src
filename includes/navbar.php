<?php
    session_start();
?>

<div id="menu">
    <div class="navbar">
        <div class="profile">
            <div class="profile_img">
                <!-- <img src="img/Avatar.svg" alt=""> -->
                <img src="img/Profile.png" alt="profile_pic">
            </div>
            <div class="profile_info">
                <h2><?php if(isset($_SESSION['name'])){
                    echo htmlspecialchars($_SESSION['name']);
                    }
                ?>
                </h2>
                <p>Administrator</p>
            </div>
        </div>
        <ul>
            <li><a href="../dashboard/dashboard.php" class="hover-link <?php if($page=='home'){ echo 'active';}?>">Dashboard</a></li>
            <li><a href="../dashboard/offers.php" class="hover-link <?php if($page=='offers'){ echo 'active';}?>">Offers</a></li>
            <li><a href="../dashboard/contact.php" class="hover-link <?php if($page=='contact'){ echo 'active';}?>">Contact</a></li>
            <li><a href="../dashboard/about.php" class="hover-link <?php if($page=='about'){ echo 'active';}?>">About</a></li>
            <li><a href="../dashboard/user.php" class="hover-link <?php if($page=='user'){ echo 'active';}?>">Users</a></li>
            <li><a href="../dashboard/admin.php" class="hover-link <?php if($page=='admin'){ echo 'active';}?>">Admin</a></li>
            <li><a href="../index.php" class="hover-link"> Show Website</a></li>
            <li><a href="../includes/logout.php" class="hover-link">Logout</a></li>
        </ul>
    </div>
    <p class="copyR">&copy; <script>document.write(new Date().getFullYear());</script>, FOOD-DELIVERY</p>
</div>
<div id="header" class="flex">
    <h2>Food-Delivery</h2>
    <img src="img/burger-logo.png" alt="logo">
</div>