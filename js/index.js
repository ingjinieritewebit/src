window.addEventListener("load", function(){
    const loader = document.querySelector(".loader");
    loader.className += " hidden";
});

function reload(){
    location.reload();
}

function order(){
    location.href = '../error_message.php';
}

function menuClick(){
    document.getElementById("burger-menu").classList.toggle("change");
    document.getElementById("nav-burger").classList.toggle("change");
}