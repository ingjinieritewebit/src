function validate(){
    if(document.myForm.subject.value.trim() == "" && document.myForm.name.value.trim() == "" && 
        document.myForm.email.value.trim() == "" && document.myForm.message.value.trim() ==""){
        alert("Nuk ka te plotesuar asnje rubrike!");
    }
    if(document.myForm.subject.value.trim() == ""){
        document.myForm.subject.focus();
        document.getElementById("subject").style.borderColor="#EE3F35";
        return false;
    }
    if(document.myForm.name.value.trim() == ""){
        document.myForm.name.focus();
        document.getElementById("name").style.borderColor="#EE3F35";
        return false;
    }
    if(document.myForm.email.value.trim() == ""){
        document.myForm.email.focus();
        document.getElementById("email").style.borderColor="#EE3F35";
        return false;
    }
    if(document.myForm.message.value.trim() ==""){
        document.myForm.message.focus();
        document.getElementById("message").style.borderColor="#EE3F35";
        return false;
    }
        
        return (true);
}
