var x = document.getElementById("login");
var y = document.getElementById("register");
var z = document.getElementById("active-btn");

function register(){
    x.style.left = "-400px";
    y.style.left = "50px";
    z.style.left = "110px";
}
function login(){
    x.style.left = "50px";
    y.style.left = "450px";
    z.style.left = "0";
}

function validateLogin(){
    var userEmail = document.myForm.email.value.trim();
    var pass = document.myForm.password.value.trim();
    if(userEmail == "" && pass == ""){
        alert("Ploteso fushat nese jeni i/e regjistruar?\nOse regjistrohuni!");
    }
    if(userEmail == ""){
        document.myForm.email.focus();
        document.getElementById("email").style.borderBottomColor="#EE3F35";
        return false;
    }
    if(pass.length < 6){
        document.myForm.password.focus();
        document.getElementById("password").style.borderBottomColor="#EE3F35";
        return false;
    }
    else{
        return (true);
    }
}

function validateRegister(){
    var userName = document.myForm2.name.value.trim();
    var email = document.myForm2.email2.value.trim();
    var pass = document.myForm2.password2.value.trim();
    if(userName == "" && email == "" && pass == ""){
        alert("Ploteso te gjitha fushat per tu regjistruar!");
    }
    if(userName == ""){
        document.myForm2.name.focus();
        document.getElementById("name").style.borderBottomColor="#EE3F35";
        return false;
    }
    if(email == ""){
        document.myForm2.email2.focus();
        document.getElementById("email2").style.borderBottomColor="#EE3F35";
        return false;
    }
    if(pass == ""){
        document.myForm2.password2.focus();
        document.getElementById("password2").style.borderBottomColor="#EE3F35";
        return false;
    }
    if(pass.length < 6){
        document.myForm2.password2.focus();
        document.getElementById("password2").style.borderBottomColor="#EE3F35";
        alert("Password-i duhet t'i kete 6 ose me shume karaktere!");
        return false;
    }
    else{
        return (true);
    }
}