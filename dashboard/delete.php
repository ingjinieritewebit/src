<?php
    require '../includes/dbconnect.php';

    if(isset($_GET['id'])){
        $id = $_GET['id'];
    }

    $query = "DELETE FROM users WHERE id = :id";
    $query = $pdo->prepare($query);

    $query->execute(['id' => $id]);
    $page = $_SERVER['PHP_SELF'];

    header("Location: dashboard.php");
?>