<?php 
  $page = 'about';
  include '../includes/navbar.php';
  require '../includes/dbconnect.php';
  $query = $pdo->query('SELECT * from about_us');
  $about_text = $query->fetchAll();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard - Food-Delivery</title>
        <link rel="icon" href="img/burger-logo.png" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
    <body>
        <div id="about" class="body_info">
            <?php foreach ($about_text as $about): ?>
                <h3 class="font" style="color: #EE3F35; font-weight: 800; font-size: 25px;"><?php echo $about['tittle']; ?></h3>
                <p><?php echo $about['content']; ?></p>
                <br>
                <br>
                <p style="color: #EE3F35; font-weight: 800;font-size: 18px;">Contact</p>
                <p>Mob: <?php echo $about['phone']; ?></p>
                <p>Email: <?php echo $about['email']; ?></p>
            <?php endforeach; ?>
            <br>
            <button class="button-add" onclick="add()">Edit</button>
        </div>
        <script>
            function add(){
                location.href = 'edit_about.php';
            }
        </script>
    </body>
</html>