<?php
    $page = 'about';
    include '../includes/navbar.php';
    require '../includes/dbconnect.php';

    // if(isset($_GET['id'])){
    //     $id = $_GET['id'];
    // }
    $sql = 'SELECT * from about_us WHERE aboutus_id = 1';
    $query = $pdo->prepare($sql);
    $query->execute(['id' => $id]);

    $about_txt = $query->fetch();

    if(isset($_POST['submit'])){
        $tittle = $_POST['tittle'];
        $content = $_POST['content'];
        $phone = $_POST['phone'];
        $email = $_POST['email'];

        $sql = 'UPDATE about_us SET tittle = :tittle, content = :content, phone = :phone, email = :email WHERE id = 1';
        $query = $pdo->prepare($sql);
        $query->bindParam('tittle', $tittle);
        $query->bindParam('content', $content);
        $query->bindParam('phone', $phone);
        $query->bindParam('email', $email);
        // $query->bindParam('id', $id);

        $query->execute();
        header("Location: about.php");
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard - Food-Delivery</title>
        <link rel="icon" href="img/burger-logo.png" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
    <body>
        <div class="body_info">
            <form action="edit_about.php" method="post" id="register" class="register-form">
                <h3>Editing about us!</h3>
                <input type="text" name="name" id="name" class="input-field first-input" value="<?php echo $about_txt['tittle']; ?>"/>
                <textarea type="text" name="content" id="email"class="input-field" ><?php echo $about_txt['content']; ?></textarea>
                <input type="text" name="phone" id="email"class="input-field" value="<?php echo $about_txt['phone']; ?>"/><br>
                <input type="email" name="email" id="email"class="input-field" value="<?php echo $about_txt['email']; ?>"/><br>
                <button type="submit" name="submit" class="submit-btn">Edit</button>
            </form>
        </div>
    </body>
</html>