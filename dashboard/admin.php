<?php 
  $page = 'admin';
  include '../includes/navbar.php';
  require '../includes/dbconnect.php';
  $query = $pdo->query('SELECT * from users WHERE role = 1');
  $users = $query->fetchAll();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard - Food-Delivery</title>
        <link rel="icon" href="img/burger-logo.png" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
    <body>
      <div class="body_info">
        <button class="button-add" onclick="add()">ADD ADMIN</button>
          <table class="table-content">
            <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Password</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($users as $user): ?>
                <tr>
                  <td><?php echo $user['name']; ?></td>
                  <td><?php echo $user['email']; ?></td>
                  <td><?php echo $user['password']; ?></td>
                  <td><a href="edit.php?id=<?php echo $user['id']; ?>">Edit</a></td>
                  <td><a href="delete.php?id=<?php echo $user['id']; ?>">Delete</a></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
    </body>
    <script>
        function add(){
            location.href = 'add_admin.php';
        }
    </script>
</html>