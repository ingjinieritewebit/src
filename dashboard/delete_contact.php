<?php
    require '../includes/dbconnect.php';

    if(isset($_GET['id'])){
        $id = $_GET['id'];
    }

    $query = "DELETE FROM contact_form WHERE cf_id = :id";
    $query = $pdo->prepare($query);

    $query->execute(['id' => $id]);
    
    header("Location: contact.php");
?>