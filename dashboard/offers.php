<?php
    $page = 'offers';
    include '../includes/navbar.php';
    require '../includes/dbconnect.php';

    $query = $pdo->query('SELECT * FROM foods');
    $foods_id = $query->fetchAll();
?>

<?php
    if(isset($_POST['submit'])){
        $price = $_POST['price'];
        $drink = $_POST['drink'];
        $food = $_POST['food'];
        $image = $_POST['image'];
        $post_author = $_POST['post_author'];

        $sql = 'INSERT INTO offers (price, drink, foods_id, image, post_author) VALUES (:price, :drink, :food, :image, :post_author)';
        $query = $pdo->prepare($sql);
        $query->bindParam('price', $price);
        $query->bindParam('drink', $drink);
        $query->bindParam('food', $food);
        $query->bindParam('image', $image);
        $query->bindParam('post_author', $post_author);
        $query->execute();
        header("Location: dashboard.php");
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard - Food-Delivery</title>
        <link rel="icon" href="img/burger-logo.png" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
    <body>
        <div class="body_info">
            <form action="offers.php" method="post" id="register" class="register-form" name="myForm">
                <h3>Adding offer!</h3>
                <input type="text" name="price" id="price" class="input-field first-input" placeholder="Price">
                <input type="text" name="drink" id="price" class="input-field first-input" placeholder="Price">
                <select name="food">
                  <?php foreach($foods_id as $foods): ?>
                    <option name="" value="<?php echo $foods['foods_id']; ?>">
                      <?php echo $foods['foods_name']; ?>
                    </option>
                  <?php endforeach; ?>
                </select>
                <label for="file" id="file-label">Chose photo</label>
                <input name="image" type="file" accept="image/*" id="file">
                <input type="hidden" name="post_author" value="<?php echo $_SESSION['name'] ?>">
                <button type="submit" name="submit" class="submit-btn">Submit</button>
            </form>
        </div>
    </body>
</html>
