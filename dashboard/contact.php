<?php 
  $page = 'contact';
  include '../includes/navbar.php';
  require '../includes/dbconnect.php';
  $query = $pdo->query('SELECT * from contact_form Order by cf_date desc');
  $contact_form = $query->fetchAll();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard - Food-Delivery</title>
        <link rel="icon" href="img/burger-logo.png" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
    <body>
      <div class="body_info">
          <table class="table-content">
            <thead>
              <tr>
                <th>Subject</th>
                <th>Name</th>
                <th>Email</th>
                <th>Message</th>
                <th>Date</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($contact_form as $contact_form): ?>
                <tr>
                  <td><?php echo $contact_form['cf_subject']; ?></td>
                  <td><?php echo $contact_form['cf_name']; ?></td>
                  <td><?php echo $contact_form['cf_email']; ?></td>
                  <td><?php echo $contact_form['cf_message']; ?></td>
                  <td><?php echo $contact_form['cf_date']; ?></td>
                  <td><a href="delete_contact.php?id=<?php echo $contact_form['cf_id']; ?>">Delete</a></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
    </body>
</html>