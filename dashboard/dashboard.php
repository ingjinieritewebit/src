<?php 
  $page = 'home';
  include '../includes/navbar.php';
  require '../includes/dbconnect.php';

  $query = $pdo->query('SELECT * FROM users WHERE role = 0');
  $users = $query->rowCount();
  $query1 = $pdo->query('SELECT * FROM users WHERE role = 1');
  $admin = $query1->rowCount();
  $query2 = $pdo->query('SELECT * FROM contact_form');
  $contact = $query2->rowCount();
  $query3 = $pdo->query('SELECT * FROM foods');
  $food = $query3->rowCount();
  $query4 = $pdo->query('SELECT * FROM offers');
  $offer = $query4->rowCount();

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard - Food-Delivery</title>
        <link rel="icon" href="img/burger-logo.png" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
    <body>
        <div class="body_info">
            <div class="widget color1">
                <h3 class="info-db"><? echo $offer ?></h3>
                <p>Offers</p>
            </div>
            <div class="widget color2">
                <h3 class="info-db"><? echo $food ?></h3>
                <p>Foods</p>
            </div>
            <div class="widget color3">
                <h3 class="info-db"><? echo $contact ?></h3>
                <p>Contact</p>
            </div>
            <br>
            <div class="widget color4">
                <h3 class="info-db"><? echo $users ?></h3>
                <p>Users</p>
            </div>
            <div class="widget color5">
                <h3 class="info-db"><? echo $admin ?></h3>
                <p>Admin</p>
            </div>
        </div>
    </body>
</html>