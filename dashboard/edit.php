<?php
    $page = 'admin';
    include '../includes/navbar.php';
    require '../includes/dbconnect.php';

    //Duhet me pas kujdes me u perdor sessioni(per qeshteje sigurie)
    if(isset($_GET['id'])){
        $id = $_GET['id'];
    }
    $sql = 'SELECT * from users WHERE id = :id';
    $query = $pdo->prepare($sql);
    $query->execute(['id' => $id]);

    $user = $query->fetch();

    if(isset($_POST['submit'])){
        $name = $_POST['name'];
        $email = $_POST['email'];

        $sql = 'UPDATE users SET name = :name, email = :email WHERE id = :id';
        $query = $pdo->prepare($sql);
        $query->bindParam('name', $name);
        $query->bindParam('email', $email);
        $query->bindParam('id', $id);

        $query->execute();
        header("Location: admin.php");
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard - Food-Delivery</title>
        <link rel="icon" href="img/burger-logo.png" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
    <body>
        <div class="body_info">
            <form method="post" id="register" class="register-form">
                <h3>Editing user!</h3>
                <input type="text" name="name" id="name" class="input-field first-input" value="<?php echo $user['name']; ?>"/>
                <input type="email" name="email" id="email"class="input-field" value="<?php echo $user['email']; ?>"/><br>
                <button type="submit" name="submit" class="submit-btn">Edit</button>
            </form>
        </div>
    </body>
</html>