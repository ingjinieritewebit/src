<?php
    $page = 'admin';
    include '../includes/navbar.php';
    require '../includes/dbconnect.php';
?>

<?php
    if(isset($_POST['submit'])){
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password1 = $_POST['password'];
        if(strlen($password1) < 6){
            $message1 = "More strong Password!";
            echo "<script type='text/javascript'>alert('$message1');</script>";
        }else{
            $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
        }
        $query = $pdo->prepare('SELECT * from users WHERE email = :email');
        $query->execute(array('email' => htmlspecialchars($_POST['email'])));
        $count = $query->rowCount();

        if($count == 0){
            $sql = 'INSERT INTO users (name, email, password, role) VALUES (:name, :email, :password, 1)';
            $query = $pdo->prepare($sql);
            $query->bindParam('name', $name);
            $query->bindParam('email', $email);
            $query->bindParam('password', $password);
            $query->execute();
            header("Location: admin.php");
        }else{
            $message = "Email excist in DB!";
            echo "<script type='text/javascript'>alert('$message');</script>";
        }
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard - Food-Delivery</title>
        <link rel="icon" href="img/burger-logo.png" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
    </head>
    <body>
        <div class="body_info">
            <form action="add_admin.php" method="post" id="register" class="register-form" name="myForm" onsubmit="return(validateRegister())">
                <h3>Adding admin!</h3>
                <input type="text" name="name" id="name"class="input-field first-input" placeholder="Name">
                <input type="email" name="email" id="email"class="input-field" placeholder="E-mail">
                <input type="password" name="password" id="password"class="input-field marginBottom" placeholder="Enter Password">
                <!-- <label for="file" id="file-label">Chose photo</label>
                <input type="file" accept="image/*" id="file"> -->
                <button type="submit" name="submit" class="submit-btn">Register</button>
            </form>
        </div>
    </body>
</html>
