<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/message.css"/>
        <link rel="icon" href="img/burger-logo.png" type="image/x-icon">
        <title>Food-Delivery - Message</title>
    </head>
    <div class="page">
        <div class="container section">
            <h1 class="hover-link">ERROR 404</h1>
            <?php
                header("refresh:5; url=http://localhost/food-delivery/index.php");
            ?>
        </div>
    </div>
</html>