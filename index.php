<?php
    session_start();

    require 'includes/dbconnect.php';
    
    $query1 = $pdo->query('SELECT * from about_us');
    $about_us_text = $query1->fetchAll();
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Food-Delivery</title>
        <link rel="icon" href="img/burger-logo.png" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="css/loader.css"/>
        <link rel="stylesheet" type="text/css" media="screen and (min-width: 320px) and (max-width: 500px)" href="css/mobile.css"/>
        <link rel="stylesheet" type="text/css" media="screen and (min-width: 501px) and (max-width:780px)" href="css/tablet.css"/>
        <link rel="stylesheet" type="text/css" href="css/style.css"/>
        <link rel="stylesheet" type="text/css" href="css/style-slider.css"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div class="loader">
            <img src="img/load.gif" alt="Loading..."/>
        </div>
        <div class="nav-container">
            <nav id="navbar" class="navbar flex">
                <ul class="flex">
                    <li class="reload" onclick="reload()"><img src="img/burger-logo.png" alt="logo"></li>
                    <li><a href="#home" class="hover-link">Home</a></li>
                    <li><a href="#menu" class="hover-link">Menu</a></li>
                    <li><a href="#offers" class="hover-link">Offers</a></li>
                    <li><a href="#contact" class="hover-link">Contact</a></li>
                    <li><a href="#about" class="hover-link">About</a></li>
                </ul>
            </nav>
            <a href="login-register.php"><img id="profile" src="img/Profile.png" alt="Profile"></a>
        </div>
        <!-- Burger-Menu -->
        <div id="burger-menu-icon">
            <img src="img/burger-logo.png" alt="logo" class="reload" onclick="reload()">
            <div id="burger-menu" onclick="menuClick()">
                <div id="bar1" class="bar"></div>
                <div id="bar2" class="bar"></div>
                <div id="bar3" class="bar"></div>
            </div>
            <ul class="nav-burger" id="nav-burger">
                <li><a href="#home">Home</a></li>
                <li><a href="#menu">Menu</a></li>
                <li><a href="#offers">Offer</a></li>
                <li><a href="#contact">Contact</a></li>
                <li><a href="#about">Aboutus</a></li>
                <li><a href="login-register.html">login</a></li>
            </ul>
        </div>

        <?php if(isset($_SESSION['name'])):?>
            <div id="logout">
                <a href="includes/logout.php">Logout</a>
            </div>
        <?php endif;?>

        <div class="full-container">
            <section id="home">
                <div class="container">
                    <div class="left-content">

                        <?php if(isset($_SESSION['name'])){
                            echo "Welcome " . htmlspecialchars($_SESSION['name']);
                            }
                        ?>
                        
                        <h2>ORDER IN WITH PICKUP OR</h2>
                        <h1 class="dev">DELIVERY!</h1>
                        <button class="button" onclick="order()">ORDER PICKUP</button>
                        <button class="button btn2" onclick="order()">ORDER DELIVERY</button>
                    </div>
                    <div class="social-media">
                        <ul>
                            <li><a href="https://www.instagram.com/?hl=en" target="_blank"><img src="img/Instagram-Logo.png" alt="IG" width="25px"></a></li>
                            <li id="fb-logo"><a href="https://www.facebook.com/" target="_blank"><img src="img/Facebook-Logo.png" alt="FB" width="13px"></a></li>
                            <li id="tw-logo"><a href="https://twitter.com/login" target="_blank"><img src="img/Twitter-Logo.png" alt="TW" width="29px"></a></li>
                        </ul>
                    </div>
                </div>
            </section>
            <section id="menu">
                <div class="container">
                    <!-- Slider V2 - Start -->
                    <div id="slider">
                        <ul>
                            <li><img src="img/img-slider/burger-fries-png-2.png"></img></li>
                            <li><img src="img/img-slider/pizza.png"></li>
                            <li><img src="img/img-slider/doner.png"></li>
                            <li><img src="img/img-slider/pomfrit.png"></li>
                          </ul>  
                    </div>
                    <script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
                    <script src="js/app.js"></script>
                    <!-- Slider V2 - End -->
                    <div id="menu" class="flex">
                        <div id="burger-img" class="food-radius" onclick="order()">
                            <h1 class="res-text">BURGER</h1>
                        </div>
                        <div id="doner-img" class="food-radius" onclick="order()">
                            <h1 class="res-text">DÖNER</h1>
                        </div>
                        <div id="pizza-img" class="food-radius" onclick="order()">
                            <h1 class="res-text">PIZZA</h1>
                        </div> 
                        <div id="sald-img" class="food-radius" onclick="order()">
                            <h1 class="res-text">SALADS</h1>
                        </div>
                    </div>
                </div>
            </section>
            <section id="offers">
                <div class="container">
                    <h1 class="offer">OFFERS</h1>
                    <div id="offer-div">
                        <div id="offerboxes">
                            <img class="img" src="img/ylli/hamburger.jpg" alt="hamburger-offer">
                            <br>
                            <br>
                            <b>$2.99</b>
                            <p>Hamburger</p>
                            <p>Coca Cola</p>
                            <button id="porosit-btn" onclick="order()">ORDER</button>
                        </div>
                        <div id="offerboxes">
                            <img class="img" src="img/ylli/doner.jpg" alt="doner-offer">
                            <br>
                            <br>
                            <b>$2.50</b>
                            <p>Donner</p>
                            <p>Coca Cola</p>
                            <button id="porosit-btn" onclick="order()">ORDER</button>
                        </div>
                        <div id="offerboxes">
                            <img class="img" src="img/ylli/pizza.jpg" alt="pizza-offer">
                            <br>
                            <br>
                            <b>$2.99</b>
                            <p>Pizza</p>
                            <p>Coca Cola</p>
                            <button id="porosit-btn" onclick="order()">ORDER</button>
                        </div>
                    </div>
                </div>
            </section>
            <section id="contact">
                <?php
                    if(isset($_POST['submit'])){
                        $subject = $_POST['subject'];
                        $name = $_POST['name'];
                        $email = $_POST['email'];
                        $message = $_POST['message'];

                        $sql = 'INSERT INTO contact_form (cf_subject, cf_name, cf_email, cf_message) VALUES (:subject, :name, :email, :message)';
                        $query = $pdo->prepare($sql);
                        $query->bindParam('subject', $subject);
                        $query->bindParam('name', $name);
                        $query->bindParam('email', $email);
                        $query->bindParam('message', $message);

                        $query->execute();
                    }
                ?>
                <div class="container">
                    <div class="contact-div">
                        <h3>Your Suggest</h3>
                        <form name="myForm" onsubmit="return(validate())" action="index.php" method="POST">
                            <input name="subject" id="subject" type="text" class="input" placeholder="Subject">
                            <input name="name" id="name" type="text" class="input" placeholder="Your Name">
                            <input name="email" id="email" type="email" class="input" placeholder="Your E-mail">
                            <textarea name="message" id="message" type="text" class="input textarea-msg" placeholder="Your Message..."></textarea>
                            <button name="submit" type="submit" class="submit-btn" id="submit_btn">Send Message</button>
                        </form>
                    </div>
                    <div class="location-div">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1467.331147194845!2d21.152807496814304!3d42.64731872728756!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13549e8d5d607f25%3A0xa31dd05b21bd09de!2sUniversiteti%20p%C3%ABr%20Biznes%20dhe%20Teknologji!5e0!3m2!1sen!2s!4v1590077736332!5m2!1sen!2s" width="500" height="510" frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
            </section>
            <section id="about">
                <div class="container">
                    <br>
                    <br>
                    <br>
                    <h1 id="aboutus">ABOUT US</h1>
                    <br class="about-br">
                    <br class="about-br">
                    <br>

                    <?php foreach ($about_us_text as $about): ?>
                        <h3 class="font-weight"><?php echo $about['tittle']; ?></h3>
                        <p><?php echo $about['content']; ?></p>
                        <br>
                        <br>
                        <p class="font-weight">Contact</p>
                        <p>Mob: <?php echo $about['phone']; ?></p>
                        <p>Email: <?php echo $about['email']; ?></p>
                    <?php endforeach; ?>

                </div>
                <footer id="footer">
                    <p>Copyright &copy; <script>document.write(new Date().getFullYear());</script>, FOOD-DELIVERY</p>
                </footer>
            </section>
        </div>
        <script src="js/index.js"></script>
        <script src="js/validate.js"></script>
    </body>
</html>