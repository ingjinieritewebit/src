<?php
      session_start();
?>

<?php
      if(isset($_SESSION['user_id'])){
          header("Location: ./index.php");
      }
  
      require 'includes/dbconnect.php';
  
    if(isset($_POST['submit'])):
        $email = $_POST['email'];
        $password = $_POST['password'];

        $query = $pdo->prepare('SELECT id, name, email, password, role FROM users WHERE email = :email');
        $query->bindParam(':email', $email);
        $query->execute();

        $user = $query->fetch();

        if(count($user) > 0 && password_verify($password, $user['password'])){
            $_SESSION['user_id'] = $user['id'];
            $_SESSION['name'] = $user['name'];
            $_SESSION['permission'] = $user['role'];
            if($_SESSION['permission'] == 1){
                header("Location: ./dashboard/dashboard.php");
            }else{
                header("Location: ./index.php");
            }

            
        }else{
            $message = "Wrong Password please try again!";
            echo "<script type='text/javascript'>alert('Food-Delivery', '$message');</script>";
        }
    endif;
?>



<?php
    if(isset($_POST['submit2'])){
        $name = $_POST['name'];
        $email2 = $_POST['email2'];
        $password2 = password_hash($_POST['password2'], PASSWORD_BCRYPT);
        $query = $pdo->prepare('SELECT * from users WHERE email = :email2');
        $query->execute(array('email2' => htmlspecialchars($_POST['email2'])));
        $count = $query->rowCount();

        if($count == 0){
            $sql = 'INSERT INTO users (name, email, password) VALUES (:name, :email2, :password2)';
            $query = $pdo->prepare($sql);
            $query->bindParam('name', $name);
            $query->bindParam('email2', $email2);
            $query->bindParam('password2', $password2);
            $query->execute();
            echo "Please login now!";
        }else{
            $message = "Registration fail! - Email excist in DB!";
            echo "<script type='text/javascript'>alert('$message');</script>";
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/login-register.css"/>
        <link rel="icon" href="img/burger-logo.png" type="image/x-icon">
        <title>Food-Delivery - Login And Register</title>
    </head>
    <body>
        <div class="page">
            <div class="page-box relative">
                <div class="button-box relative">
                    <div id="active-btn"></div>
                    <button type="button" class="toggle-btn relative" onclick="login()">Logi In</button>
                    <button type="button" class="toggle-btn relative" onclick="register()">Register</button>
                </div>
                <form action="login-register.php" method="POST" onsubmit="return(validateLogin())" id="login" class="input-group" name="myForm">
                    <h3>Welcome!</h3>
                    <input name="email" id="email" type="text" class="input-field input1" placeholder="E-mail">
                    <input name="password" id="password" type="password" class="input-field" placeholder="Enter Password">
                    <input type="checkbox" class="check-box"><span>Remember Password</span>
                    <button name="submit" type="submit" class="submit-btn">Log In</button>
                </form>
                <form id="register" class="input-group" name="myForm2" onsubmit="return(validateRegister())" action="login-register.php" method="POST">
                    <h3>Register now!</h3>
                    <input name="name" id="name" type="text" class="input-field input1" placeholder="Your name">
                    <input name="email2" id="email2" type="email" class="input-field" placeholder="Your E-mail">
                    <input name="password2" id="password2" type="password" class="input-field" placeholder="Enter Password">
                    <input type="checkbox" class="check-box"><span>I agree to the terms & conditions</span>
                    <button name="submit2" type="submit" class="submit-btn">Register</button>
                </form>
                <div class="social-icons relative">
                    <a href="https://www.instagram.com/?hl=en" target="_blank"><img src="img/Instagram-Logo.png" alt="IG" width="25px"></a>
                    <a href="https://www.facebook.com/" target="_blank"><img src="img/Facebook-Logo.png" alt="FB" width="13px"></a>
                    <a href="https://twitter.com/login" target="_blank"><img src="img/Twitter-Logo.png" alt="TW" width="29px" id="tw"></a>
                </div>
            </div>
        </div>
        <script src="js/validateLoginAndRegister.js"></script>
    </body>
</html>